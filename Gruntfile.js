module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		watch: {
			build: {
				files: ['src/*.js', '!src/build.js*'],
				tasks: ['clean', 'traceur']
			}
		},
		clean: {
			build: ['src/build.js*']
		},
		traceur: {
			build: {
				src: ['src/*.js'],
				dest: 'src/build.js'
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-traceur');

	grunt.registerTask('default', ['watch']);
};