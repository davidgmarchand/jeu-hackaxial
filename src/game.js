angular.module("app").factory("game", function($interval) {
	function Game() {
		this.money = 10000;
		this.costs = 0;
		this.tick = 1500;

		this.resources = [
			{
				name: "Alex",
				cost: 100,
				skill: 50,
				hired: false
			},
			{
				name: "Dave",
				cost: 150,
				skill: 60,
				hired: false
			}
		];

		this.projects = [];

		$interval(() => {
			var availableResources = this.resources.filter(r => r.hired).length;

			this.projects.filter((project) => {
				return availableResources >= project.resources && !project.completed;
			}).forEach((function(project) {
				project.completedTime += 1;
				availableResources -= project.resources;

				if (project.completedTime == project.duration) {
					project.completed = true;
					this.money += project.value;
				}
			}).bind(this));

			this.money -= this.costs;
		}, this.tick);

		this.recalculateCosts = function() {
			this.costs = this.resources.reduce((previous, res) => {
				return previous + (Number(res.hired) * res.cost);
			}, 0);
		};
	}

	return new Game();
});