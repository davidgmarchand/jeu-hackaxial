"use strict";
angular.module("app", []);
;
angular.module("app").factory("game", function($interval) {
  function Game() {
    var $__0 = this;
    this.money = 10000;
    this.costs = 0;
    this.tick = 1500;
    this.resources = [{
      name: "Alex",
      cost: 100,
      skill: 50,
      hired: false
    }, {
      name: "Dave",
      cost: 150,
      skill: 60,
      hired: false
    }];
    this.projects = [];
    $interval((function() {
      var availableResources = $__0.resources.filter((function(r) {
        return r.hired;
      })).length;
      $__0.projects.filter((function(project) {
        return availableResources >= project.resources && !project.completed;
      })).forEach((function(project) {
        project.completedTime += 1;
        availableResources -= project.resources;
        if (project.completedTime == project.duration) {
          project.completed = true;
          this.money += project.value;
        }
      }).bind($__0));
      $__0.money -= $__0.costs;
    }), this.tick);
    this.recalculateCosts = function() {
      this.costs = this.resources.reduce((function(previous, res) {
        return previous + (Number(res.hired) * res.cost);
      }), 0);
    };
  }
  return new Game();
});
;
angular.module("app").controller("InfoCtrl", function($scope, game) {
  $scope.game = game;
});
;
angular.module("app").controller("ProjectsCtrl", function($scope, game) {
  $scope.projects = game.projects;
});
;
angular.module("app").controller("ProjectsListingCtrl", function($scope, game) {
  $scope.listing = [];
  $scope.listing.push({
    name: "Site Web",
    value: 10000,
    duration: 15,
    resources: 2
  });
  $scope.accept = function($index) {
    var project = $scope.listing.splice($index, 1)[0];
    project.completedTime = 0;
    game.projects.push(project);
  };
});
;
angular.module("app").controller("ResourcesCtrl", function($scope, game) {
  $scope.resources = game.resources;
  $scope.hire = function(index) {
    game.resources[index].hired = true;
    game.recalculateCosts();
  };
});

//# sourceMappingURL=build.js.map
