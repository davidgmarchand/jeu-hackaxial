angular.module("app").controller("ProjectsListingCtrl", function($scope, game) {
	$scope.listing = [];

	$scope.listing.push({
		name: "Site Web",
		value: 10000,
		duration: 15,
		resources: 2
	});

	$scope.accept = function($index) {
		var project = $scope.listing.splice($index, 1)[0];
		project.completedTime = 0;
		game.projects.push(project);
	};
});