angular.module("app").controller("ResourcesCtrl", function($scope, game) {
	$scope.resources = game.resources;

	$scope.hire = function(index) {
		game.resources[index].hired = true;
		game.recalculateCosts();
	};
});